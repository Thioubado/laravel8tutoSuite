<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\video18Controller;
use App\Http\Controllers\video19Controller;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// route pous la video 18
Route::get('video18', [video18Controller::class,'getData']);

// route pous la video 18
Route::get('video19', [video19Controller::class,'index']);