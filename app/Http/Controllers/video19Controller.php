<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// import du HTTP
use Illuminate\Support\Facades\Http;

class video19Controller extends Controller
{
    //
    function index()
    {
        $collection = Http::get("https://reqres.in/api/users?page=1");
        return view('video19',['collection'=>$collection]);
    }
}
